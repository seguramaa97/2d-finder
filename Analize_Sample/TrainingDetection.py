 #Library importation
import numpy as np
import os
import cv2
import pandas as pd
from skimage.filters import scharr as scharr_filter, prewitt as prewitt_filter
from scipy.ndimage import gaussian_filter
import time
import glob
from matplotlib import pyplot as plt
import shutil
import xgboost as xgb
import catboost as cat
from sklearn.ensemble import AdaBoostClassifier
from tqdm import tqdm



#Extract Features from microscopy images and convert into different gaussian filters

def extract_image_features(image):
    features_dataframe = pd.DataFrame()

    # Convert to HSV and split the channels
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hue_channel, saturation_channel, value_channel = cv2.split(hsv_image)
    features_dataframe['Hue'] = hue_channel.flatten()
    features_dataframe['Saturation'] = saturation_channel.flatten()
    features_dataframe['Value'] = value_channel.flatten()
    
    # Convert to grayscale and reshape
    grayscale_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    features_dataframe['Grayscale_Intensity'] = grayscale_image.flatten()
    
    # Apply Scharr filter
    scharr_edges = scharr_filter(grayscale_image).flatten()
    features_dataframe['Scharr_Edges'] = scharr_edges

    # Apply Prewitt filter
    prewitt_edges = prewitt_filter(grayscale_image).flatten()
    features_dataframe['Prewitt_Edges'] = prewitt_edges

    # Apply Gaussian filters with different sigma values
    gaussian_blur_sigma3 = gaussian_filter(grayscale_image, sigma=3).flatten()
    features_dataframe['Gaussian_Blur_Sigma3'] = gaussian_blur_sigma3

    gaussian_blur_sigma7 = gaussian_filter(grayscale_image, sigma=7).flatten()
    features_dataframe['Gaussian_Blur_Sigma7'] = gaussian_blur_sigma7
    
    return features_dataframe


#Create Dataframe with Filter labels as column and each pixel as arrow.
def create_dataframe_with_labels(image, label_image):
    extracted_features = extract_image_features(image)
    
    # Process labels
    processed_label = cv2.cvtColor(label_image, cv2.COLOR_BGR2GRAY)
    #binarize images in grayscale colour
    processed_label[processed_label > 0] = 1
    extracted_features['Label'] = processed_label.flatten()
    
    return extracted_features


#Function to process the training dataset. image_directory is where all the image training pictures are stored , and Mask_directory where the Label mask are stored.
def process_image_and_label(image_directory,Mask_directory):
    combined_dataframe = pd.DataFrame()
    #Loads all the images
    for image_file in glob.glob(image_directory):
        print(image_file)
        image_name = os.path.basename(image_file).split('.')[0]
        print(image_name)
        
        # Load and resize the image
        original_image = cv2.imread(image_file)
        resized_image = cv2.resize(original_image, (original_image.shape[1] // 4, original_image.shape[0] // 4))
        
        # Load and resize the corresponding label image
        label_image_path = os.path.join(f'{Mask_directory}', f'{image_name}.png')
        label_image = cv2.imread(label_image_path)
        resized_label_image = cv2.resize(label_image, (label_image.shape[1] // 4, label_image.shape[0] // 4))
        
        # Extract features and create a dataframe
        image_features_dataframe = create_dataframe_with_labels(resized_image, resized_label_image)
        combined_dataframe = pd.concat([combined_dataframe, image_features_dataframe])

    return combined_dataframe


#boolean Funtion to detect monolayers. Minimum threshold size for detecting a monolayer.#
def check_flake_presence(tol, segmented_img):
    tol=tol*27.81
    #27.81 is the constant px/μm2
    """
    Determines if a segmented image contains a monolayer
    by checking if the number of non-zero pixels (white pixels of the prediction mask)
    exceeds a specified tolerance.
    
    """
    return np.count_nonzero(segmented_img) >= tol



#Function for training a model, different tree based algorithms can be used: XGB, CAT, ADA.
def train_model(training_directory, Mask_directory, model_type='XGB'):
    """
    Trains a classifier model using the features extracted from images 
    located in the training directory and its associated Mask directory.
    The type of model to train is determined by the model_type parameter.
    
    
    """
    df_features_labels = process_image_and_label(training_directory, Mask_directory)
    
    # Prepare the training data and labels
    X_train = df_features_labels.drop(labels=["Label"], axis=1)
    Y_train = df_features_labels["Label"].values
    #Create a global variable to store the model
    global trained_model
    # Train the specified model
    if model_type == 'XGB':
        trained_model = xgb.XGBClassifier()
        
    elif model_type == 'CAT':
        trained_model = cat.CatBoostClassifier(verbose = False)
        
    elif model_type == 'ADA':
        trained_model = AdaBoostClassifier(verbose = False)
    else:
        raise ValueError("Invalid model type specified. Choose 'XGB' or 'Cat'.")
    
    trained_model.fit(X_train, Y_train)
    return trained_model

    
    
    
#Function to search for monolayers inside a folder. Optionally the prediction made by the model can be shown and/or saved.
def identify_monolayers(image_path_pattern, tol,show_prediction=False,save_prediction=False):
    """
    Identifies monolayers in images located at the specific path, 
    or sample path, tolerance here is a minimum flake size set by the user.
    """
    detected_monolayers = []
    for image_path in tqdm(glob.glob(image_path_pattern)):
        file_number = image_path.split(os.path.sep)[-1].split('.')[0]
        # Load and preprocess the image
        image = cv2.imread(image_path)
        resized_image = cv2.resize(image, (image.shape[1] // 2, image.shape[0] // 2))
        
        # Extract features from the preprocessed image
        features = extract_image_features(resized_image)
        
        # Convert image to grayscale for further operations
        grayscale_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
        
        # Predict the presence of a monolayer using the trained model
        prediction = trained_model.predict(features)
        
        # Process the prediction result to form a segmented image
        
        segmented_image = prediction.reshape((grayscale_image.shape))
        segmented_image = cv2.resize(segmented_image, (resized_image.shape[1], resized_image.shape[0]))

        if show_prediction:
            print(file_number)
            plt.imshow(segmented_image, cmap='gray')
            plt.axis('off')
            plt.show()
            
        if save_prediction:
            if not os.path.exists('prediction_mask'):
                os.makedirs('prediction_mask')
            plt.imsave('prediction_mask/'+file_number+'.png', segmented_image, cmap ='gray')
            
        
        # Check if the segmented image meets the criteria for having a monolayer
        if check_flake_presence(tol, segmented_img=segmented_image):
            # Extract and append the image identifier if a monolayer is detected
            image_identifier = int(image_path.split(os.path.sep)[-1].split('.')[0])
            detected_monolayers.append(image_identifier)
    
    return detected_monolayers

#Copy detected monolayers to an specific folder
def copy_monolayers(monolayer_index,sample_path,detected_path):
    for file in monolayer_index:

        file=str(file)
        if not file.endswith('.jpg'):
            file += '.jpg'

        full_sample_path = os.path.join(sample_path, file)
        full_detected_path = os.path.join(detected_path, file)

        shutil.copy(full_sample_path, full_detected_path)
    
    print("Monolayers succesfully copied.")






