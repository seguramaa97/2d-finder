 #Library importation
import numpy as np
import os
import cv2
import pandas as pd
import skimage
from skimage.filters import scharr as scharr_filter, prewitt as prewitt_filter
from scipy.ndimage import gaussian_filter
from sklearn import metrics
import time
import glob
import pickle
from matplotlib import pyplot as plt
from scipy.stats import mode
from scipy import ndimage
import shutil
from skimage import measure
from sklearn.metrics import confusion_matrix
import lightgbm as lgb
import xgboost as xgb
from sklearn.ensemble import RandomForestClassifier
import catboost as cat
import shutil

from sklearn.ensemble import AdaBoostClassifier

from tqdm import tqdm




def extract_image_features(image):
    features_dataframe = pd.DataFrame()

    # Convert to HSV and split the channels
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hue_channel, saturation_channel, value_channel = cv2.split(hsv_image)
    features_dataframe['Hue'] = hue_channel.flatten()
    features_dataframe['Saturation'] = saturation_channel.flatten()
    features_dataframe['Value'] = value_channel.flatten()
    
    # Convert to grayscale and reshape
    grayscale_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    features_dataframe['Grayscale_Intensity'] = grayscale_image.flatten()
    
    # Apply Scharr filter
    scharr_edges = scharr_filter(grayscale_image).flatten()
    features_dataframe['Scharr_Edges'] = scharr_edges

    # Apply Prewitt filter
    prewitt_edges = prewitt_filter(grayscale_image).flatten()
    features_dataframe['Prewitt_Edges'] = prewitt_edges

    # Apply Gaussian filters with different sigma values
    gaussian_blur_sigma3 = gaussian_filter(grayscale_image, sigma=3).flatten()
    features_dataframe['Gaussian_Blur_Sigma3'] = gaussian_blur_sigma3

    gaussian_blur_sigma7 = gaussian_filter(grayscale_image, sigma=7).flatten()
    features_dataframe['Gaussian_Blur_Sigma7'] = gaussian_blur_sigma7
    
    return features_dataframe


def create_dataframe_with_labels(image, label_image):
    extracted_features = extract_image_features(image)
    
    # Process labels
    processed_label = cv2.cvtColor(label_image, cv2.COLOR_BGR2GRAY)
    processed_label[processed_label > 0] = 1
    extracted_features['Label'] = processed_label.flatten()
    
    return extracted_features


def process_image_and_label(image_directory,Mask_directory):
    combined_dataframe = pd.DataFrame()
    
    for image_file in glob.glob(image_directory):
        print(image_file)
        image_name = os.path.basename(image_file).split('.')[0]
        print(image_name)
        
        # Load and resize the image
        original_image = cv2.imread(image_file)
        resized_image = cv2.resize(original_image, (original_image.shape[1] // 4, original_image.shape[0] // 4))
        
        # Load and resize the corresponding label image
        label_image_path = os.path.join(f'{Mask_directory}', f'{image_name}.png')
        label_image = cv2.imread(label_image_path)
        resized_label_image = cv2.resize(label_image, (label_image.shape[1] // 4, label_image.shape[0] // 4))
        
        # Extract features and create a dataframe
        image_features_dataframe = create_dataframe_with_labels(resized_image, resized_label_image)
        combined_dataframe = pd.concat([combined_dataframe, image_features_dataframe])

    return combined_dataframe


def check_flake_presence(tol, segmented_img):
    tol=tol*27.81
    #27.81 is the constant px/μm2
    """
    Determines if a segmented image contains a monolayer
    by checking if the number of non-zero pixels (white pixels of the prediction mask)
    exceeds a specified tolerance.
    
    """
    return np.count_nonzero(segmented_img) > tol


def train_model(training_directory, Mask_directory, model_type='XGB'):
    """
    Trains a classifier model using the features extracted from images 
    located in the training directory and its associated Mask directory.
    The type of model to train is determined by the model_type parameter.
    
    
    """
    df_features_labels = process_image_and_label(training_directory, Mask_directory)
    
    # Prepare the training data and labels
    X_train = df_features_labels.drop(labels=["Label"], axis=1)
    Y_train = df_features_labels["Label"].values
    #Create a global variable to store the model
    global trained_model
    # Train the specified model
    if model_type == 'XGB':
        trained_model = xgb.XGBClassifier()
        
    elif model_type == 'CAT':
        trained_model = cat.CatBoostClassifier(verbose = False)
        
    elif model_type == 'ADA':
        trained_model = AdaBoostClassifier(verbose = False)
    else:
        raise ValueError("Invalid model type specified. Choose 'XGB' or 'Cat'.")
    
    trained_model.fit(X_train, Y_train)
    return trained_model

    
    
    

def identify_monolayers(image_path_pattern, tol,show_prediction=False,save_prediction=False):
    """
    Identifies monolayers in images located at the specific path, 
    or sample path, tolerance here is a minimum flake size set by the user.
    """
    detected_monolayers = []
    for image_path in tqdm(glob.glob(image_path_pattern)):
        file_number = image_path.split(os.path.sep)[-1].split('.')[0]
        # Load and preprocess the image
        image = cv2.imread(image_path)
        resized_image = cv2.resize(image, (image.shape[1] // 2, image.shape[0] // 2))
        
        # Extract features from the preprocessed image
        features = extract_image_features(resized_image)
        
        # Convert image to grayscale for further operations
        grayscale_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
        
        # Predict the presence of a monolayer using the trained model
        prediction = trained_model.predict(features)
        
        # Process the prediction result to form a segmented image
        
        segmented_image = prediction.reshape((grayscale_image.shape))
        segmented_image = cv2.resize(segmented_image, (resized_image.shape[1], resized_image.shape[0]))

        if show_prediction:
            print(file_number)
            plt.imshow(segmented_image, cmap='gray')
            plt.axis('off')
            plt.show()
            
        if save_prediction:
            if not os.path.exists('prediction_mask'):
                os.makedirs('prediction_mask')
            plt.imsave('prediction_mask/'+file_number+'.png', segmented_image, cmap ='gray')
            
        
        # Check if the segmented image meets the criteria for having a monolayer
        if check_flake_presence(tol, segmented_img=segmented_image):
            # Extract and append the image identifier if a monolayer is detected
            image_identifier = int(image_path.split(os.path.sep)[-1].split('.')[0])
            detected_monolayers.append(image_identifier)
    
    return detected_monolayers


def is_monolayer(flake_size,capture_img):
    # Load and preprocess the image
    image_np = np.array(capture_img)
    image = cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR)  # Convertir de RGB a BGR
    resized_image = cv2.resize(image, (image.shape[1] // 2, image.shape[0] // 2))
    
    # Extract features from the preprocessed image
    features = extract_image_features(resized_image)
    
    # Convert image to grayscale for further operations
    grayscale_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
    
    # Predict the presence of a monolayer using the trained model
    prediction = trained_model.predict(features)
    
    # Process the prediction result to form a segmented image
    
    segmented_image = prediction.reshape((grayscale_image.shape))
    segmented_image = cv2.resize(segmented_image, (resized_image.shape[1], resized_image.shape[0]))
    return check_flake_presence(flake_size, segmented_image)
    


def copy_monolayers(monolayer_index,sample_path,detected_path):
    for file in monolayer_index:
        # Añadir el sufijo .jpg si no está presente
        file=str(file)
        if not file.endswith('.jpg'):
            file += '.jpg'

        full_sample_path = os.path.join(sample_path, file)
        full_detected_path = os.path.join(detected_path, file)

        # Copiar el archivo
        shutil.copy(full_sample_path, full_detected_path)
    
    print("Monolayers succesfully copied.")


###################################################################################################################################
###################################################################################################################################
#Scanning Part#
###################################################################################################################################
###################################################################################################################################


#Library importation
import cv2
import numpy as np
import os
from PIL import ImageGrab
import TangoStage as TS
import NikonFocus as NF
import keyboard
import matplotlib.pyplot as plt
from tqdm import tqdm
import time
import json


def initialize_stage(X = 42.6616 ,Y= 10.9664,R = 331.6296 ,initial_focus = 630):
    #Stage initial values
    #X = 42.6616
    #Y = 10.9664
    #R = 331.6296
    #initial_focus = 630
    print("Initializing Stage...")
    TS.MoveXYRAbsolute(X,Y,R)
    NF.MoveZAbsolute(initial_focus)
    time.sleep(1)
    print("Done.")
    
    
#Function to capture the microscopy images, by taking screenshots from Nikon NIS Camera software.
def capture():
    bbox = (235, 163, 1600, 1100)
    sct_img = ImageGrab.grab(bbox)
    return sct_img

#Function to set the limits of the sample. 
#the limits will be stored in a selected "path"

def sample_limits():
    global positions_edges
    positions_edges = []  # List to store XY positions
    edges = ['top left', 'top right', 'bottom right']  # Descriptions of each point
    print("Set the x10 Objective")
    for edge in edges:
        print(f"Place the microscope on the sample's {edge} edge.")
        
        print("Press any key to save position")
        
        keyboard.read_event()  # Wait for a key press
        
        try:
            position = TS.XYPosition()  # Assuming TS.XYPosition() returns a tuple (X, Y)
            print(f"{edge.capitalize()} edge position: ", position)
            positions_edges.append(position)
            
        except Exception as e:
            print(f"An error occurred while getting position for {edge} edge: {e}")
            positions_edges.append((None, None))  # Append None values if there's an error
    # Create the fourth element of the square        
    positions_edges.append((positions_edges[0][0],positions_edges[2][1]))
    # Calculate deltaX and deltaY assuming the positions are [(top_left_x, top_left_y), (top_right_x, top_right_y), (bottom_right_x, bottom_right_y)]
    deltaX = abs(positions_edges[1][0] - positions_edges[0][0])
    deltaY = abs(positions_edges[2][1] - positions_edges[0][1])
    
    
    print(f"The sample has a length of {deltaX} [mm].")
    print(f"And a width of {deltaY} [mm].")
    print(f"Total Area to scan {deltaX*deltaY} [mm^2].")

    # Optionally, move the microscope back to the top left position
    try:
        TS.GoTo(*positions_edges[0])  # Unpack the top left position and move there
    except Exception as e:
        print(f"An error occurred while moving to the top left position: {e}")

    return positions_edges
   

#Sample goes back to the limits previosly set with the function sample_limits
#This time the Z-axis can be moved to fix the different focus points level of the sample, enabling surface linear fitting to be utilized as an autofocus system.
def focus_edges(position_edges):
    points = []
    i=1
    for position in position_edges:
        TS.GoTo(*position)
        print(f"Point {i}: Search and focus on a monolayer/thin layer")
        print(f"Press any key to save point {i} XYZ Information")
        
        keyboard.read_event()  # Wait for a key press
        
        try:
            xyz = NF.XYZPosition()  # Assuming NF.XYZPosition() returns a tuple (X, Y, Z)
            print(f"Point {i} X Y Z Coordinates: ", xyz)
            points.append(xyz)
        except Exception as e:
            print(f"An error occurred while getting XYZ position for point {i}: {e}")
            points.append((None, None, None))  # Append None values if there's an error
        i+=1
        
    X, Y, Z = zip(*points)  # This unpacks the list of tuples into three tuples  
    
    return X,Y,Z
        


#Plot the surface scanned, it combines the points colledted by sample_limits and focus_edges functions

def surface_plot(X, Y, Z):
    # Create a figure and an axis
    plt.figure(figsize=(8, 6))

    # Use the z value for color, using a color map
    scatter = plt.scatter(X, Y, c=Z, cmap='viridis')

    # Add a color bar to indicate how colors relate to z values
    plt.colorbar(scatter, label='Z Axis [µm]')

    # Add titles and labels
    plt.title("Focus Points Scanned")
    plt.xlabel("X Axis [Unit]")
    plt.ylabel("Y Axis [Unit]")

    # Display the plot
    plt.show()



def surface_fit(X, Y, Z):
    """
    Perform multivariable linear regression to model Z as a function of X and Y.
    
    Parameters:
    - X: A list or array of x-values.
    - Y: A list or array of y-values.
    - Z: A list or array of z-values (dependent variable).
    
    Returns:
    - coefficients: A tuple containing the coefficients (m1, m2) and the intercept (c).
    """

    # Convert lists to numpy arrays for efficient computation
    x = np.array(X)
    y = np.array(Y)
    z = np.array(Z)

    # Stack x and y for multivariable regression
    xy = np.column_stack((x, y))

    # Prepare the design matrix for linear regression by adding a column of ones for the intercept
    A = np.hstack([xy, np.ones((xy.shape[0], 1))])

    # Perform the linear regression using the least squares method
    try:
        coefficients, residuals, rank, singular_values = np.linalg.lstsq(A, z, rcond=None)
        global m1,m2,c
        m1, m2, c = coefficients  # Unpack the coefficients
    except np.linalg.LinAlgError:
        print("An error occurred during the linear regression computation.")
        return None

    return m1, m2, c




#autofocus function, evaluates the surface function for a given position x,y using the calculated fit coefficients m1,m2,c
def z_pred(x,y):
    return np.round(m1 * x + m2 * y + c,1)

#Funtion that reads the position and adjust the focus to the linear fit
def focus_autocorrection():
    x,y = TS.XYPosition()   
    z = z_pred(x,y)
    NF.MoveZAbsolute(z)
    

#Function to generate a matrix of values to scan the sample from top left corner to top right corner, until the bottom right limit.
def generate_matrix(x, y):
    xy = []
    # Indicator to toggle the order of x
    order_ascending = True
    
    # Traverse the values of y in descending order
    for yi in sorted(y, reverse=True):
        # Toggle the order of x between ascending and descending
        x_ordered = sorted(x) if order_ascending else sorted(x, reverse=True)
        for xi in x_ordered:
            xy.append((xi, yi))
        # Change the order for the next iteration
        order_ascending = not order_ascending

    return xy


#Generate an array of Scanning positions, based on the sample limits and objective capture range.
def generate_array(start, end, increment):
    if increment <= 0:
        print("The increment must be greater than zero.")
        return []

    array = []
    current_value = start

    while current_value <= end:
        array.append(round(current_value, 4))
        current_value += increment

    return array



#Scan the sample until a monolayer that satiesfies the threshold size is found.
def Scan_until_find(flake_size,position_edges, objective='x100',relax_time=1):
    #Objectives length and width scanning ranges in mm
    objectives = {
        'x100': [0.1373, 0.1004],
        'x50': [0.27730, 0.19779],
    }
    #reads the possible objetive range, from the previously selected objective
    xrange = objectives[objective][0]
    yrange = objectives[objective][1]
    #Generate the position arrays for the x and y axis based on the limits and the objective maximum range.
    x = generate_array(position_edges[0][0], position_edges[1][0], xrange)
    y = generate_array(position_edges[2][1], position_edges[0][1], yrange)
    #generate the matrix with all the scanning elements 
    xy = generate_matrix(x, y)

    positions = xy
    print("Total Number of steps: ", len(xy)  + 1)
    print("Press 's' to cancel the scanning")

    try:
        for i in tqdm(xy, desc="Scanning Sample", unit="iteration"):
            if keyboard.is_pressed('s'):  
                print("Scanning cancelled.")
                break  # stop the scanning by pressing c
            #Moves the position to the i component of the matrix
            TS.GoTo(*i)
            #Autofocus using the linear surface function
            NF.MoveZAbsolute(z_pred(*i))
            #temporal pause to let the mechanical parts move before capturing the microscopy image
            time.sleep(relax_time)
            #Capture image from the microscope 
            temp_img=capture()
            #analyse if the sample image has a flake satisfying the minimum flake size selected
            if is_monolayer(flake_size, temp_img):
                print("Monolayer Found")
                #After the first monolayer is found it can continue to find another monolayer or the program can be cancelled by pressing c
                print("press any key to continue")
                keyboard.read_event()
                
                    
        
    except KeyboardInterrupt:
        # Maneja la excepción para evitar errores si el proceso se interrumpe
        print("Scanning stopped manually.")






    
    
    






