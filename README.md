# 2D Finder

   2D finder is a tool to train and detect a desired type of 2D Material (e.g MoS2 monolayer).
There are three main jupyter notebook proyects with different functions:

-Analyze_Sample
-Sample_Autoscanning_Tool
-Sample_SimultaneousScan+Detection

Additionaly there is a Dataset folder, with all the MoS2 flakes used for training and test on the PDMS and SiO2/Si substrates. 



## Analyze_Sample
It can train a flake classifier with three based machine learning algorithms: AdaBoost, Catboost or XGBoost.
The main.ipynyb jupiter notebook files using the TrainingDetection.py library to work.

Selecting the folders where the training and Mask pictures are stored, and the model type to train, it can later detect flakes of a  minimal desired size in µm2.
The detected flakes from a "test" folder are stored indepently in a separated "detected" folder. 

## Sample_Sample_Autoscanning_Tool

It is capable of controlling the motorized XY stage (TangoStage.py) and the microscope focus systems (NikonLV.py and NikonFocus.py) to automatically scan a sample and capture all the microscopy images.

The AutoScan.py library contains the functions that captures the images, saves the information about the sample limits and focus points, generates a scanning positions matrix, among others.

The program can be started by opening the Main.ipynyb file on jupyter notebook.

## Sample_SimultaneousScan+Detection

It is a combination of the AutoScan.py and TrainingDetection.py libraries, allowing the creation of a program that can scan a sample and in iteration check if there is a flake, based on a previous training dataset.

When the desired flakes is found, the program either stops completly or can continue until finding the next flake.

For its working also uses the same libraries TangoStage.py, NikonLV.py and NikonFocus.py for the communication with motorized stage and microscope focus.





