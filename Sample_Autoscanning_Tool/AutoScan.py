#Library importation

import cv2
import numpy as np
from mss import mss
import os
from PIL import ImageGrab
import TangoStage as TS
import NikonFocus as NF
import keyboard
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from tqdm import tqdm
import time

import json


#Function to capture the microscopy images, by taking screenshots from Nikon NIS Camera software.

def capture(path):
    global img_counter
    bbox = (235, 163, 1600, 1100)
    sct_img = ImageGrab.grab(bbox)
    sct_img.save(f"{path}{img_counter}.jpg")
    #Every picture has an unique number asociated to it.
    img_counter+=1



def save_array(array, file_name):
    """
    Saves an array in a file in JSON format.

    Args:
    - array: The array to save.
    - file_name: The name or path of the file where the array will be saved.
    """
    with open(file_name, 'w', encoding='utf-8') as file:
        json.dump(array, file)
        
    
    
def load_array(file_name):
    """
    Loads an array from a JSON file.

    Args:
    - file_name: The name or path of the JSON file to read.

    Returns:
    The array read from the JSON file.
    """
    with open(file_name, 'r', encoding='utf-8') as file:
        array = json.load(file)
    return array



#Initialize stage: x,y,R and Focus position
def initialize_stage(X = 42.6616 ,Y= 10.9664,R = 331.6296 ,initial_focus = 630):
    #Stage initial values
    #X = 42.6616
    #Y = 10.9664
    #R = 331.6296
    #initial_focus = 630
    print("Initializing Stage...")
    TS.MoveXYRAbsolute(X,Y,R)
    NF.MoveZAbsolute(initial_focus)
    time.sleep(1)
    print("Done.")
    
    
#Function to set the limits of the sample. 
#the limits will be stored in a selected "path"
def sample_limits(path):
    global positions_edges
    positions_edges = []  # List to store XY positions
    edges = ['top left', 'top right', 'bottom right']  # Descriptions of each point
    print("Set the x10 Objective")
    for edge in edges:
        print(f"Place the microscope on the sample's {edge} edge.")
        
        print("Press any key to save position")
        
        keyboard.read_event()  # Wait for a key press
        
        try:
            position = TS.XYPosition()  # Assuming TS.XYPosition() returns a tuple (X, Y)
            print(f"{edge.capitalize()} edge position: ", position)
            positions_edges.append(position)
            
        except Exception as e:
            print(f"An error occurred while getting position for {edge} edge: {e}")
            positions_edges.append((None, None))  # Append None values if there's an error
    # Create the fourth element of the square        
    positions_edges.append((positions_edges[0][0],positions_edges[2][1]))
    # Calculate deltaX and deltaY assuming the positions are [(top_left_x, top_left_y), (top_right_x, top_right_y), (bottom_right_x, bottom_right_y)]
    deltaX = abs(positions_edges[1][0] - positions_edges[0][0])
    deltaY = abs(positions_edges[2][1] - positions_edges[0][1])
    
    
    print(f"The sample has a length of {deltaX} [mm].")
    print(f"And a width of {deltaY} [mm].")
    print(f"Total Area to scan {deltaX*deltaY} [mm^2].")

    # Optionally, move the microscope back to the top left position
    try:
        TS.GoTo(*positions_edges[0])  # Unpack the top left position and move there
    except Exception as e:
        print(f"An error occurred while moving to the top left position: {e}")

    save_array(positions_edges, path + "positions_edges.json")
    print("Edge Positions array saved as positions_edges.json")
    return positions_edges

#Sample goes back to the limits previosly set with the function sample_limits
#This time the Z-axis can be moved to fix the different focus points level of the sample, enabling surface linear fitting to be utilized as an autofocus system.

def focus_edges(position_edges,path):
    points = []
    i=1
    #it goes to every saved position
    for position in position_edges:
        TS.GoTo(*position)
        print(f"Point {i}: Search and focus on a monolayer/thin layer")
        print(f"Press any key to save point {i} XYZ Information")
        
        keyboard.read_event()  # Wait for a key press to save the Focus information
        
        try:
            xyz = NF.XYZPosition()  # Assuming NF.XYZPosition() returns a tuple (X, Y, Z)
            print(f"Point {i} X Y Z Coordinates: ", xyz)
            points.append(xyz)
        except Exception as e:
            print(f"An error occurred while getting XYZ position for point {i}: {e}")
            points.append((None, None, None))  # Append None values if there's an error
        i+=1
        
    X, Y, Z = zip(*points)  # This unpacks the list of tuples into three tuples
    save_array(X,path + "X_Focus.json")
    save_array(X,path + "Y_Focus.json")
    save_array(Z,path + "Z_Focus.json")
    print("Surface information saved as X,Y,Z.json")  
    
    return X,Y,Z
        
#Plot the surface scanned, it combines the points colledted by sample_limits and focus_edges functions

def surface_plot(X, Y, Z):
    # Create a figure and an axis
    plt.figure(figsize=(8, 6))

    # Use the z value for color, using a color map
    scatter = plt.scatter(X, Y, c=Z, cmap='viridis')

    # Add a color bar to indicate how colors relate to z values
    plt.colorbar(scatter, label='Z Axis [µm]')

    # Add titles and labels
    plt.title("Focus Points Scanned")
    plt.xlabel("X Axis [Unit]")
    plt.ylabel("Y Axis [Unit]")

    # Display the plot
    plt.show()



def surface_fit(X, Y, Z):
    """
    Perform multivariable linear regression to model Z as a function of X and Y.
    
    Parameters:
    - X: A list or array of x-values.
    - Y: A list or array of y-values.
    - Z: A list or array of z-values (dependent variable).
    
    Returns:
    - coefficients: A tuple containing the coefficients (m1, m2) and the intercept (c).
    """

    # Convert lists to numpy arrays for efficient computation
    x = np.array(X)
    y = np.array(Y)
    z = np.array(Z)

    # Stack x and y for multivariable regression
    xy = np.column_stack((x, y))

    # Prepare the design matrix for linear regression by adding a column of ones for the intercept
    A = np.hstack([xy, np.ones((xy.shape[0], 1))])

    # Perform the linear regression using the least squares method
    try:
        coefficients, residuals, rank, singular_values = np.linalg.lstsq(A, z, rcond=None)
        global m1,m2,c
        m1, m2, c = coefficients  # Unpack the coefficients
    except np.linalg.LinAlgError:
        print("An error occurred during the linear regression computation.")
        return None

    return m1, m2, c

#autofocus function, evaluates the surface function for a given position x,y using the calculated fit coefficients m1,m2,c
def z_pred(x,y):
    return np.round(m1 * x + m2 * y + c,1)

#Funtion that reads the position and adjust the focus to the linear fit
def focus_autocorrection():
    x,y = TS.XYPosition()   
    z = z_pred(x,y)
    NF.MoveZAbsolute(z)
    

#Preset the image counter to 0
img_counter=0



#Function to generate a matrix of values to scan the sample from top left corner to top right corner, until the bottom right limit.
def generate_matrix(x, y):
    xy = []
    # Indicator to toggle the order of x
    order_ascending = True
    
    # Traverse the values of y in descending order
    for yi in sorted(y, reverse=True):
        # Toggle the order of x between ascending and descending
        x_ordered = sorted(x) if order_ascending else sorted(x, reverse=True)
        for xi in x_ordered:
            xy.append((xi, yi))
        # Change the order for the next iteration
        order_ascending = not order_ascending

    return xy

#Generate an array of Scanning positions, based on the sample limits and objective capture range.
def generate_array(start, end, increment):
    if increment <= 0:
        print("The increment must be greater than zero.")
        return []

    array = []
    current_value = start

    while current_value <= end:
        array.append(round(current_value, 4))
        current_value += increment

    return array


#Tool for Scanning the sample
def ScanRangev4(position_edges, path, objective='x100',relax_time=1 ):
    #Objectives length and width scanning ranges in mm
    objectives = {
        'x100': [0.1373, 0.1004],
        'x50': [0.27730, 0.19779],
    }

    #reads the possible objetive range, from the previously selected objective
    xrange = objectives[objective][0]
    yrange = objectives[objective][1]
    #Generate the position arrays for the x and y axis based on the limits and the objective maximum range.
    x = generate_array(position_edges[0][0], position_edges[1][0], xrange)
    y = generate_array(position_edges[2][1], position_edges[0][1], yrange)
    #generate the matrix with all the scanning elements 
    xy = generate_matrix(x, y)

    positions = xy
    print("Total Number of steps: ", len(xy)  + 1)
    print("Press 'c' to cancel the scanning")

    try:
        for i in tqdm(xy, desc="Scanning Sample", unit="iteration"):
            if keyboard.is_pressed('c'):  #Scanning can be manually stoped by pressing c.
                print("Scanning cancelled.")
                break
            #Moves the position to the i component of the matrix
            TS.GoTo(*i)
            #Autofocus using the linear surface function
            NF.MoveZAbsolute(z_pred(*i))
            #temporal pause to let the mechanical parts move before capturing the microscopy image
            time.sleep(relax_time)
            #Capture image from the microscope 
            capture(path)
            
        
    except KeyboardInterrupt:
        # save the current array if the scanning is manually stoped
        print("Scanning stopped manually.")
        save_array(positions, path+"sample_positions.json")
        print("Pictures position saved in file sample_positions.json")
        #return an array of positions , which index correspond to the image number
    return positions



def create_folder(original_path, folder_name):
    try:
        # Combine the original path with the folder name
        folder_path = os.path.join(original_path, folder_name)
        
        # Try to create the folder
        os.mkdir(folder_path)
        print(f"Folder '{folder_path}' created successfully.")
    except FileExistsError:
        # If the folder already exists, print a warning message
        print(f"Folder '{folder_path}' already exists.")
        
        
       
#after Scaning the sample, go to a desired captured flake by introducing the index of the image.
def GoToPicture(picture_index,positions):
    TS.GoTo(*positions[picture_index])




    
    
    
